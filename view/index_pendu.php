<?php
session_start();
if (!$_SESSION) {
    include '../util/reset.php';
}
//var_dump($_SESSION);
?>

<html>
    <head>
        <title>Jeu du pendu - PokerStars</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link type="text/css" rel="stylesheet" href="../css/bootstrap.css">
        <link type="text/css" rel="stylesheet" href="../css/base.css">
    </head>
    <body>
        <div>
            <div id="pendu">
                <?php include '../controller/pendu_controller.php'; ?>
            </div>
            <div>
                <fieldset class="well" id="well">
                    <?php
                    if (isset($_SESSION['word_displayed'])) {
                        foreach ($_SESSION['word_displayed'] as $letter) {
                            echo $letter;
                        }
                    }
                    ?>
                </fieldset>
            </div><br>
            <form action="../controller/check_lettre.php" method="post">
                <div id="row">
                    <?php include '../controller/keyboard_controller.php'; ?>
                </div>
            </form>
            <form action="../util/reset.php" method="post">
                <div id="row">
                    <button id="reset" type="submit" class="btn btn-primary">
                        <span class="glyphicon glyphicon-repeat"></span>
                    </button>
                </div>
            </form>
<!--<a href="../util/reset.php"><image src="../ressources/restart.png" title="Recommencer la partie" name="reset" width=50/></a>-->
        </div>
    </body>
</html>