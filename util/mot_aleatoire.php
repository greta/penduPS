<?php

//Initialisation d'une table avec 30 mots de 6 à 8 lettres
function mot_aleatoire() {
    $tab_mots = ["ALCOOL", "ACHETEUR", "JOURNAL", "BILLET", "LAVABO", "DEMAIN", "TABLIER", "VENDREDI", "CUISINE",
        "SEMAINE", "CULTURE", "CHIFFRE", "COMMERCE", "SERVICE", "MINUTE", "PATRON", "LOGEMENT", "STOCKAGE", "ESCALIER",
        "GRATUITE", "BRADERIE", "MAISON", "JARDIN", "FESTIVAL", "ENSEMBLE", "GENTIL", "NOUVELLE", "SOLIDE",
        "FAMILLE", "PRODUIT"];
//Génération d'un nombre aléatoire puis d'un mot aléatoire
    $aleatoire = mt_rand(0, count($tab_mots));
    return $tab_mots[$aleatoire];
}
