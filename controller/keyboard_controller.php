<?php

include '../util/Button.php';

$_SESSION['selected_letter'];
$btn_success = "btn btn-success";
$btn_default = "btn btn-default";
$btn_loose = "btn btn-danger";
$bouton = new Button();
for ($i = 65; $i <= 90; $i++) :
    if ((in_array(chr($i), $_SESSION['selected_letter'])) && (in_array(chr($i), $_SESSION['word_array']))) :
        echo $bouton->input($btn_success, chr($i));
    elseif (in_array(chr($i), $_SESSION['selected_letter'])) :
        echo $bouton->input($btn_loose, chr($i));
    else :
        echo $bouton->input($btn_default, chr($i));
    endif;
    if ($i == 73 || $i == 82) :
        echo "<br>";
    endif;
endfor;
