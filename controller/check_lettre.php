<?php

session_start();

//// Etape 1 : définition de la fonction de génération d'un mot aléatoire
//
//--- pas de code supplémentaire à insérer dans ce fichier pour cette étape ----
//
include '../util/mot_aleatoire.php';

if (!isset($_SESSION['word'])) {
    $_SESSION['vies'] = 8;
    $_SESSION['word'] = mot_aleatoire();
}

$word = $_SESSION['word'];
//$word = "CACAHUETE";
//
//// Etape 2 : saisie d'une lettre par l'utilisateur
//
$lettre = filter_input(INPUT_POST, 'lettre');
$_SESSION['selected_letter'][] = $lettre;
//$lettre = "T";
//
//// Etape 3 : traitement de la lettre saisie puis renvoi de données pour affichage correspondant
//
$_SESSION['word_array'] = str_split($word); //Je transforme la chaine de charactère en un tableau de lettres.
$word_array = $_SESSION['word_array'];
//
if (!isset($_SESSION['word_displayed'])) {
    for ($compteur = 0; $compteur < strlen($word); $compteur++) {
        if ($compteur == strlen($word) - 1) {
            $word_displayed[] = "_";
        } else {
            $word_displayed[] = "_ ";
        }
    }
    $_SESSION['word_displayed'] = $word_displayed;
} else {
    $word_displayed = $_SESSION['word_displayed'];
}

if ($_SESSION['vies'] > 0) {
    if (in_array($lettre, $word_array)) {
        $array_keys = array_keys($word_array, $lettre);
        foreach ($array_keys as $key) {
            if ($key == strlen($word) - 1) {
                $word_displayed[$key] = $lettre;
            } else {
                $word_displayed[$key] = $lettre . " ";
            }
        }

        $_SESSION['word_displayed'] = $word_displayed;
//
//------------------------------------------------------------------------------
//// Etape 4 : en cas de saisie d'une lettre non contenue dans le mot, décrementation du nombre de "vies" restantes et affichages correspondants
//
    } else {
        $_SESSION['vies'] --;
    }
//
//------------------------------------------------------------------------------
//// Etape 5 : définition d'un bouton et d'une fonction de réinitialisation du jeu
//
//--- pas de code supplémentaire à insérer dans ce fichier pour cette étape ----
//--- l'appel du fichier contenant la fonction se fait depuis l'attribut "action" du bouton reset de "index_pendu.php"
//
//------------------------------------------------------------------------------
//// Etape 6 : affichage du message PERDU/GAGNE
//
//    $string = implode('', $word_displayed); // Transforme $word_displayed en string
//    $array = explode(' ', $string);         // Supprime tous les espaces puis transforme $string en tableau
//
//    if ($word_array === $array) {           // Compare $array avec le tableau du mot à trouver
//        $_SESSION['result'] = 1;
//    } else {
//        $_SESSION['result'] = 0;
//    }

    if (!in_array("_ ", $word_displayed) && !in_array("_", $word_displayed)) {
        $_SESSION['result'] = 1;
    }
}

if ($_SESSION['vies'] == 1) {
    $_SESSION['word_displayed'] = $_SESSION['word_array'];
}
//
//------------------------------------------------------------------------------
header('Location:../view/index_pendu.php');
